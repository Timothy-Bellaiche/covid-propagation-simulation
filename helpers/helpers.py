import numpy as np
from scipy.spatial import distance

def unit_vector(v):
    return v / np.linalg.norm(v)

def manhattan_distance(A, B):
    xa, ya = A
    xb, yb = B
    dist = abs(xa - xb) + abs(ya - yb)

    return dist


def euclidean_distance(A, B):
    return distance.euclidean(A, B)
