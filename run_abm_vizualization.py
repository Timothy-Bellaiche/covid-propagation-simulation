from visualization.server import server
import asyncio
import faulthandler

faulthandler.enable()

# python 3.8 has some problems with tornado
# to make it work on windows, we have to change the loop policy
if not isinstance(asyncio.get_event_loop_policy(), asyncio.WindowsSelectorEventLoopPolicy):
    asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())


server.launch()
