import numpy as np
from pprint import pprint
import matplotlib.pyplot as plt

from abm.movement.navmesh import Navmesh, Graph
from abm.environments.shapes import complex_shape

shape = complex_shape()


navmesh = Navmesh(shape)


navmesh.plot_navmesh()

path = navmesh.astar_search((1.5, 9), (12.5, 3.5))
navmesh.plot_path(path)