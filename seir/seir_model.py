import numpy as np
from scipy.integrate import odeint

def deriv(y, t, N, beta, gamma,kappa,delta,alpha,nu,rho):
    S, E, I, Q, R, D = y
    dSdt = -beta * S * I/N
    dEdt = beta * S * I / N - alpha * E
    #dIdt = alpha*(1-kappa) * E - gamma* I # Si on met pas death 
    #dQdt = alpha*kappa * E  - delta*Q
    #dRdt = delta*Q + gamma*I
    dIdt = alpha*(1-kappa) * E - gamma* I
    dQdt = alpha*kappa * E  - (1-nu)*delta*Q -nu * rho*Q
    dRdt = (1 - nu) * delta*Q + gamma*I
    dDdt = nu * rho * Q
    return dSdt, dEdt, dIdt, dQdt, dRdt, dDdt

def integration_equations(deriv,y0,t,N,beta,gamma,kappa,delta,alpha,nu,rho):
    # Integrate the SIR equations over the time grid, t.
    ret = odeint(deriv, y0, t, args=(N, beta, gamma,kappa,delta,alpha,nu,rho))
    return ret
