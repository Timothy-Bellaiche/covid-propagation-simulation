from mesa.visualization.ModularVisualization import VisualizationElement

# Used to build the custom Histogram Viz module
# Uses the HistogramModule.js file
class HistogramModule(VisualizationElement):
    package_includes = ["Chart.min.js"]
    local_includes = ["visualization/HistogramModule.js"]

    def __init__(self, canvas_height, canvas_width):
        self.canvas_height = canvas_height
        self.canvas_width = canvas_width
        new_element = "new HistogramModule({}, {})"

        new_element = new_element.format(canvas_width,
                                         canvas_height)
        self.js_code = "elements.push(" + new_element + ");"
    
    def render(self, model):
        infection_origins = model.schedule.get_infection_origins()
        labels = [key for key in infection_origins]
        infections = [infection_origins[key] for key in infection_origins]
        
        data = {"labels": labels, "data": infections}
        
        return data
        # hist = np.histogram(infection_origins, bins=self.bins)[0]
        # return [int(x) for x in hist]

