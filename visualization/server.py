from mesa.visualization.modules import ChartModule

from visualization.ModularServer import ModularServer, VisualizationElement

from visualization.ContinuousSpaceVisualization import ContinuousSpace
from abm.parameters import shape_parameters_for_visualization
from abm.agents import Human, Virus
from abm.model import CovidContagion
from abm.environments.shapes import complex_shape
from visualization.HistogramVisualization import HistogramModule

def agent_portrayal(agent):
    if agent is None:
        return

    portrayal = {"Shape": "circle",
                 "Filled": "true"}

    if agent.agent_type == "human":
        portrayal["Color"] = "blue"
        portrayal["Layer"] = 1
        if agent.hands_infected:
            portrayal["Color"] = "purple"
            portrayal["Layer"] = 2
        if agent.infected:
            portrayal["Color"] = "rgba(255, 0, 0, 1)"
            portrayal["Layer"] = 3
        portrayal["r"] = 0.5

    elif agent.agent_type == "virus":
        portrayal["Color"] = "rgba(55, 255, 55, 0.25)"
        portrayal["r"] = 2 * agent.radius # r is actually diameter...
        portrayal["Layer"] = 4
        portrayal["text"] = agent.duration
        portrayal["text_color"] = "black"

    return portrayal

#TODO TEMP shape details

shape = complex_shape()

# END TEMP shape details

canvas_element = ContinuousSpace(agent_portrayal, shape["width"], shape["height"], shape["width"]*40, shape["height"]*40, shape)


infection_rate_chart = ChartModule([{"Label": "Infected Ratio", "Color": "red"}, {"Label": "Hands Infected Ratio", "Color": "purple"}])

infection_origin_histogram = HistogramModule(150, 400)

server = ModularServer(CovidContagion, 
                      [canvas_element, infection_rate_chart, infection_origin_histogram], 
                      "Office COVID-19 spread simulation",
                      model_params = shape_parameters_for_visualization(),
                      interactive_params = {})
                    #   interactive_params=parameters_list(parameters))