var HistogramModule = function(canvas_width, canvas_height) {
    // Create the tag:
    var canvas_tag = "<canvas width='" + canvas_width + "' height='" + canvas_height + "' ";
    canvas_tag += "style='border:1px dotted'></canvas>";
    // Append it to #elements:
    var canvas = $(canvas_tag)[0];
    $("#elements").append(canvas);
    // Create the context and the drawing controller:
    var context = canvas.getContext("2d");

    // Prep the chart properties and series:
    var datasets = [{
        data: [],
        backgroundColor: 'rgba(255, 99, 132, 0.2)',
        borderColor:'rgba(255, 99, 132, 1)',
        label: "# of infected"
    }];

    // Add a zero value for each bin
    // for (var i in bins)
    //     datasets[0].data.push(0);

    var data = {
        labels: [],
        datasets: datasets
    };

    var options = {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }

    // Create the chart object
    var chart = new Chart(context, {type: 'bar', data: data, options: options});

    // Render the chart
    this.render = function(render_data) {
        datasets[0].data = render_data.data;
        data.labels = render_data.labels
        chart.update();
    };

    // Reset function to restart simulation
    this.reset = function() {
        chart.destroy();
        chart = new Chart(context, {type: 'bar', data: data, options: options});
    };
};