from api import run_model

from abm.model import Scenario, CovidContagion
from abm.agents import InfectionOrigin
from abm.parameters import Parameters, shape_parameters_for_model_simulations

import matplotlib.pyplot as plt


infection_ratio = 0
hands_infection_ratio = 0
n_sneeze_infection = 0
n_touch_infection = 0
iters = 50



for i in range(iters):
    p = Parameters()

    p.scenario_duration["value"] = 600
    p.scenario["value"] = Scenario.EXIT_AFTER_DURATION

    formatted_p = shape_parameters_for_model_simulations(p)

    model = CovidContagion(**formatted_p) 
    model.run_model()
    df = model.datacollector.get_model_vars_dataframe()
    series = df.iloc[len(df) - 1]
    infection_ratio += series["Infected Ratio"]
    hands_infection_ratio += series["Hands Infected Ratio"]
    if InfectionOrigin.SNEEZE in series["Infection Origins"]:
        n_sneeze_infection += series["Infection Origins"][InfectionOrigin.SNEEZE]
    if InfectionOrigin.TOUCHED_OWN_FACE in series["Infection Origins"]:    
        n_touch_infection += series["Infection Origins"][InfectionOrigin.TOUCHED_OWN_FACE]

infection_ratio /= iters
hands_infection_ratio /= iters
n_touch_infection /= iters
n_sneeze_infection /= iters

df = model.datacollector.get_model_vars_dataframe()
print(df)
plt.figure()
df = df.drop(["Infection Origins"], axis=1) # remove since not plotable
df.plot()
plt.show()