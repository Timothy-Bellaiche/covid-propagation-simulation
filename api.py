from abm.model import CovidContagion
from abm.parameters import Parameters, shape_parameters_for_model_simulations



def run_model(formated_parameters):
    #formated_parameters = shape_parameters_for_model_simulations(parameters) # create **kwargs input for model init

    model = CovidContagion(**formated_parameters) 

    model.run_model()

    df = model.datacollector.get_model_vars_dataframe()
    return df
