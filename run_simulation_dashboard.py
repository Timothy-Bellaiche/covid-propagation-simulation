from abm.model import CovidContagion
from abm.parameters import Parameters, shape_parameters_for_model_simulations
from abm.model import Scenario
from abm.agents import InfectionOrigin
import numpy as np
import dash
import pandas as pd
import dash_core_components as dcc
import dash_html_components as html
import json
from dash.dependencies import Input, Output
import plotly.graph_objs as go
import pathlib
from dash.exceptions import PreventUpdate
import statistics
import dash_daq as daq
from scipy.integrate import odeint
from api import run_model
from seir.seir_model import deriv, integration_equations

app = dash.Dash(
    __name__,
    meta_tags=[{"name": "viewport",
                "content": "width=device-width, initial-scale=1"}],
)
server = app.server
app.config["suppress_callback_exceptions"] = True

APP_PATH = str(pathlib.Path(__file__).parent.resolve())
colors = {
    'background': 'black',
    'text': 'white'
}
markdown_text1 = '''
#### Prediction of the covid19 epidemic spread in an office. 
'''


def build_banner():
    return html.Div(
        id="banner",
        className="banner",
        children=[
            html.Div(
                id="banner-text",
                children=[
                    html.H4(
                        "COVID-19 spread simulation in an office"),
                ],
            ),

            html.Div(
                id="banner-logo",
                children=[
                    html.Img(id="logo", src=app.get_asset_url(
                        "logo_Capgemini.jpg")),
                ],
            ),
        ],
    )


app.layout = html.Div(
    children=[
        build_banner(),
        html.Div(
            id="big-app-container",
            children=[
                # header
                html.H3('1 - Simulating a work then exit to cafeteria scenario', className="color-blue"),
                #html.H3('1 - Simuler un scénario', className="color-blue"),
                html.P(
                    'To obtain the result and statistics for the scenario given its parameters'),
                #html.P('Pour obtenir le résultat et les statistiques d\'un scénario aux paramètres donnés'),
                html.Div(html.P([html.Br()])),

                html.Div([
                    html.Div([
                        html.H5('Simulation\'s parameters'),
                        #html.H5('Paramètres de simulation'),
                        html.Div([html.Label('Number of simulations'),
                                  # html.Div([html.Label('Nombre de simulations'),
                                  dcc.Input(id='nb_simulation',
                                            value='50', min=0, type='number'),
                                  html.Div(id='my_nb_simulation')]),
                        html.Div([html.Label('Duration of work before moving to cafeteria (in mins)'),
                                  # html.Div([html.Label('Durée d\'une récréation (en mins)'),
                                  dcc.Input(id='duree_simu', value='10',
                                            min=0, type='number'),
                                  html.Div(id='my_duree_simu')
                                  ]),
                        html.Div([html.Label("Number of employees"),
                                  # html.Div([html.Label("Nombre d'élèves"),
                                  dcc.Input(id='nb_children', value='20',
                                            min=1, type='number'),
                                  html.Div(id='my_nb_children')
                                  ]),
                        html.Div([html.Label("Number of initially infected employees"),
                                  # html.Div([html.Label("Nombre d'élèves initialement infectés"),
                                  dcc.Input(id='nb_children_inf',
                                            value='1', min=0, type='number'),
                                  html.Div(id='my_nb_children_inf')
                                  ]),
                        html.Div([
                            daq.BooleanSwitch(
                                id='mask_use',
                                on=False,
                                color="#008000",
                                label="Wearing a mask",
                                #label="Port du masque",
                                labelPosition="top",
                                className="two columns",
                                style=dict(display='flex',
                                           justifyContent='left')
                            ),
                            html.Div(id='my_mask_use')
                        ]),
                        html.Div(
                            html.P([html.Br(), html.Br(), html.Br(), html.Br()])),
                        html.Div(html.Button(
                            'Launch the simulation', id='button')),
                        #html.Div(html.Button('Lancer la simulation', id='button')),
                        html.Div(html.P([html.Br()])),
                        html.Div(id='intermediate-value',
                                 style={'display': 'none'}),

                    ], className="three columns"),

                    html.Div([
                        html.Div([html.H5('Evolution of the infection rate'),
                                  # html.Div([html.H5('Evolution du taux d\'infection'),
                                  html.P(id="titre1",
                                         children="")]),

                        dcc.Graph(id='g1', figure={
                            'data': [],
                            'layout': dict(
                                xaxis={'title': 'Time (min)'},
                                #xaxis={'title': 'Temps (secondes)'},
                                yaxis={'title': 'Infection rate'},
                                #yaxis={'title': 'Taux d\'infection'},
                                height=400,  # px
                                width=400
                            )

                        })], className="four columns"),
                    html.Div([
                        html.Div([html.H5('Origin of the infections'),
                                  # html.Div([html.H5('Origine des infections'),
                                  html.P(id="titre2",
                                         children="")
                                  ]),
                        dcc.Graph(id='g2', figure={
                            'data': [],
                            'layout': dict(
                                xaxis={'title': 'Origin of the infections'},
                                #xaxis={'title': 'Origine d\'infection'},
                                yaxis={'title': 'Number of infected'},
                                #yaxis={'title': 'Nombre d\'infectés'},
                                height=400,  # px
                                width=400)
                        })
                    ], className="four columns"),
                ], className="row"),
                html.H3(
                    '__________________________________________________________________________________________________________', className="color-blue"),
                html.H3('2 - Compare two scenarios', className="color-blue"),
                #html.H3('2 - Comparer deux scénarios', className="color-blue"),
                html.P('To obtain the effectiveness of the mask wearing measure'),
                #html.P('Pour obtenir l\'éfficacité de la mesure du port du masque'),
                html.Div(html.P([html.Br()])),
                html.Div([html.Div([
                    html.Div(html.Button('Comparer', id='compare_button'),
                             ), ], className="three columns"),
                          html.Div([html.Div([html.H5('Evolution of the infection rate'),
                                              # html.Div([html.Div([html.H5('Evolution du taux d\'infection'),
                                              html.P(id="titre3",
                                                     children="")
                                              ]),
                                    dcc.Graph(id='g1_compare', figure={
                                        'data': [],
                                        'layout': dict(
                                            xaxis={'title': 'Time (min)'},
                                            #xaxis={'title': 'Temps (secondes)'},
                                            yaxis={'title': 'Infection rate'},
                                            #yaxis={'title': 'Taux d\'infection'},
                                            height=400,  # px
                                            width=400
                                        )

                                    })], className="four columns"),
                          html.Div([
                              html.Div([html.H5('Origin of the infections'),
                                        # html.Div([html.H5('Origine des infections'),
                                        html.P(id="titre4",
                                               children="")
                                        ]),
                              dcc.Graph(id='g2_compare', figure={
                                  'data': [],
                                  'layout': dict(
                                      xaxis={
                                          'title': 'Origin of the infections'},
                                      #xaxis={'title': 'Origine d\'infection'},
                                      yaxis={'title': 'Number of infected'},
                                      #yaxis={'title': 'Nombre d\'infectés'},
                                      height=400,  # px
                                      width=400
                                  )

                              })], className="four columns"),

                          ], className="row"),
                html.Div([html.Div([html.H5(''),
                                    html.P(id="titre9",
                                           children="")
                                    ], className="three columns"),
                          html.Div([html.H5(''),
                                    html.P(id="titre5",
                                           style={'horizontal-align': 'right'},
                                           children="")
                                    ], className="eight columns")]),
                html.H3(
                    '__________________________________________________________________________________________________________', className="color-blue"),
                html.H3('3 - Measuring the impact on the population',
                        # html.H3('3 - Mesurer l\'impact sur la population',
                        className="color-blue"),
                html.P(
                    'Projecting in time the impact of the scenarios on the global population using the statistics obtained previously'),
                #html.P('Se projeter dans le temps sur l\'impact des scénarios sur la population globale en utilisant les statistiques obtenues précédemment'),
                html.Div(html.P([html.Br()])),
                html.Div([
                    html.Div(
                        html.Div(
                            html.Button('Projection in time',
                                        # html.Button('Projection dans le temps',
                                        id='seir_model')
                        ),
                        className="three columns"),
                    html.Div([
                        html.Div([html.H5('Projection without mask'),
                                  # html.Div([html.H5('Projection sans masque'),
                                  html.P(id="titre6",
                                         children="")
                                  ]),
                        dcc.Graph(id='g1_seir', figure={
                            'data': [],
                            'layout': dict(
                                xaxis={'title': 'Time (days)'},
                                #xaxis={'title': 'Temps (jours)'},
                                yaxis={'title': 'Number'},
                                #yaxis={'title': 'Nombre'},
                                height=400,  # px
                                width=400
                            )

                        })], className="four columns"),
                    html.Div([
                        html.Div([html.H5('Projection with mask'),
                                  # html.Div([html.H5('Projection avec masque'),
                                  html.P(id="titre7",
                                         children="")
                                  ]),
                        dcc.Graph(id='g2_seir', figure={
                            'data': [],
                            'layout': dict(
                                xaxis={'title': 'Time (days)'},
                                #xaxis={'title': 'Temps (jours)'},
                                yaxis={'title': 'Number'},
                                #yaxis={'title': 'Nombre'},
                                height=400,  # px
                                width=400
                            )

                        })], className="four columns"),
                ], className="row"),
                html.P([html.Br(), html.Br(), html.Br(), html.Br()])

            ]
        )
    ])


@app.callback(
    Output('titre1', 'children'),
    [Input('intermediate-value', 'children'),
     Input('button', 'n_clicks'),
     Input('nb_simulation', 'value'),
     Input('duree_simu', 'value'),
     Input('nb_children', 'value'),
     Input('nb_children_inf', 'value'),
     Input('mask_use', 'on')])
def up_titre1(jsonified_cleaned_data, n_clicks, nb_simulation, duree_simu, nb_children, nb_children_inf, mask_use):
    if not n_clicks:
        raise PreventUpdate
    datasets = json.loads(jsonified_cleaned_data)
    df_n = pd.read_json(datasets['df_tot_statistics'], orient='split')
    df_n.drop('Infection Origins', axis=1, inplace=True)
    resulttable_calc = df_n.groupby(level=0)
    df_stats = resulttable_calc.agg(['mean', 'std', 'median'])
    df_stats.columns = ['Infected Ratio mean', 'Infected Ratio std', 'Infected Ratio median',
                        'Hands Infected Ratio mean', 'Hands Infected Ratio std', 'Hands Infected Ratio median']
    df_stats.reindex(columns=sorted(df_stats.columns))
    dict_row = {'Infected Ratio mean': float(nb_children_inf)/float(nb_children), 'Infected Ratio std': 0,
                'Infected Ratio median': float(nb_children_inf)/float(nb_children), 'Hands Infected Ratio mean': 0,
                'Hands Infected Ratio std': 0, 'Hands Infected Ratio median': 0}
    row_df = pd.DataFrame(dict_row, index=[0])
    df_min = pd.concat([row_df, df_stats], ignore_index=True)
    df_minutes = df_min.loc[df_min.index % 60 == 0, :]

    infected_ratio = round(df_minutes.iloc[-1]['Infected Ratio mean']*100, 1)
    return "On average {}% of the class pupils will be infected in total after a break of {} minutes".format(infected_ratio, duree_simu)
    # return "En moyenne, {}% des élèves de la classe seront infectés au total après une récréation durant {} minutes".format(infected_ratio, duree_simu)


@app.callback(
    Output('titre2', 'children'),
    [Input('intermediate-value', 'children'),
     Input('button', 'n_clicks'),
     Input('nb_simulation', 'value'),
     Input('duree_simu', 'value'),
     Input('nb_children', 'value'),
     Input('nb_children_inf', 'value'),
     Input('mask_use', 'on')])
def up_titre2(jsonified_cleaned_data, n_clicks, nb_simulation, duree_simu, nb_children, nb_children_inf, mask_use):
    if not n_clicks:
        raise PreventUpdate
    datasets = json.loads(jsonified_cleaned_data)
    origin_infection = pd.read_json(datasets['res_infected'], orient='split')

    new_infections = round(
        origin_infection.iloc[-1]['Touched own face mean']+origin_infection.iloc[-1]['sneeze mean'], 1)
    new_infections_face = round(
        origin_infection.iloc[-1]['Touched own face mean'], 1)
    new_infections_sneeze = round(origin_infection.iloc[-1]['sneeze mean'], 1)
    return "On average, {} pupils were infected after a break: {} by touching their own face and {} by sneezing".format(new_infections, new_infections_face, new_infections_sneeze)
    # return "En moyenne, {} élèves seront infectés après une récréation dont {} en touchant leur visage et {} par éternuement".format(new_infections, new_infections_face, new_infections_sneeze)


@app.callback(
    Output('titre3', 'children'),
    [Input('intermediate-value', 'children'),
     Input('compare_button', 'n_clicks'),
     Input('nb_simulation', 'value'),
     Input('duree_simu', 'value'),
     Input('nb_children', 'value'),
     Input('nb_children_inf', 'value'),
     Input('mask_use', 'on')])
def up_titre3(jsonified_cleaned_data, n_clicks, nb_simulation, duree_simu, nb_children, nb_children_inf, mask_use):
    if not n_clicks:
        raise PreventUpdate
    datasets = json.loads(jsonified_cleaned_data)
    df_n = pd.read_json(datasets['df_tot_statistics'], orient='split')
    df_n.drop('Infection Origins', axis=1, inplace=True)
    resulttable_calc = df_n.groupby(level=0)
    df_stats = resulttable_calc.agg(['mean', 'std', 'median'])
    df_stats.columns = ['Infected Ratio mean', 'Infected Ratio std', 'Infected Ratio median',
                        'Hands Infected Ratio mean', 'Hands Infected Ratio std', 'Hands Infected Ratio median']
    df_stats.reindex(columns=sorted(df_stats.columns))
    dict_row = {'Infected Ratio mean': float(nb_children_inf)/float(nb_children), 'Infected Ratio std': 0,
                'Infected Ratio median': float(nb_children_inf)/float(nb_children), 'Hands Infected Ratio mean': 0,
                'Hands Infected Ratio std': 0, 'Hands Infected Ratio median': 0}
    row_df = pd.DataFrame(dict_row, index=[0])
    df_min = pd.concat([row_df, df_stats], ignore_index=True)
    df_minutes = df_min.loc[df_min.index % 60 == 0, :]

    df_n_compare = pd.read_json(
        datasets['df_tot_statistics_compare'], orient='split')
    df_n_compare.drop('Infection Origins', axis=1, inplace=True)
    resulttable_calc_compare = df_n_compare.groupby(level=0)
    df_stats_compare = resulttable_calc_compare.agg(['mean', 'std', 'median'])
    df_stats_compare.columns = ['Infected Ratio mean', 'Infected Ratio std', 'Infected Ratio median',
                                'Hands Infected Ratio mean', 'Hands Infected Ratio std', 'Hands Infected Ratio median']
    df_stats_compare.reindex(columns=sorted(df_stats.columns))
    dict_row_compare = {'Infected Ratio mean': float(nb_children_inf)/float(nb_children), 'Infected Ratio std': 0,
                        'Infected Ratio median': float(nb_children_inf)/float(nb_children), 'Hands Infected Ratio mean': 0,
                        'Hands Infected Ratio std': 0, 'Hands Infected Ratio median': 0}
    row_df_compare = pd.DataFrame(dict_row_compare, index=[0])
    df_min_compare = pd.concat(
        [row_df_compare, df_stats_compare], ignore_index=True)
    df_minutes_compare = df_min_compare.loc[df_min_compare.index % 60 == 0, :]

    infected_ratio = df_minutes.iloc[-1]['Infected Ratio mean']
    infected_ratio_compare = df_minutes_compare.iloc[-1]['Infected Ratio mean']

    max_infection = max([infected_ratio, infected_ratio_compare])
    min_infection = min([infected_ratio, infected_ratio_compare])
    return "The infection rate without a mask is {} and with a mask is {}, which is {} lower".format(round(max_infection, 3), round(min_infection, 3), round(max_infection/min_infection, 2))
    # return "Le  taux d'infection sans masque est de {} et avec masque de {}, ce qui est {} fois plus bas, et cela pour une récréation de {} minutes".format(round(max_infection, 3), round(min_infection, 3), round(max_infection/min_infection, 2),duree_simu)


@app.callback(
    Output('titre4', 'children'),
    [Input('intermediate-value', 'children'),
     Input('compare_button', 'n_clicks'),
     Input('nb_simulation', 'value'),
     Input('duree_simu', 'value'),
     Input('nb_children', 'value'),
     Input('nb_children_inf', 'value'),
     Input('mask_use', 'on')])
def up_titre4(jsonified_cleaned_data, n_clicks, nb_simulation, duree_simu, nb_children, nb_children_inf, mask_use):
    if not n_clicks:
        raise PreventUpdate
    datasets = json.loads(jsonified_cleaned_data)
    origin_infection = pd.read_json(datasets['res_infected'], orient='split')
    origin_infection_compare = pd.read_json(
        datasets['res_infected_compare'], orient='split')

    new_infections = round(
        origin_infection.iloc[-1]['Touched own face mean']+origin_infection.iloc[-1]['sneeze mean'], 1)
    new_infections_compare = round(
        origin_infection_compare.iloc[-1]['Touched own face mean']+origin_infection_compare.iloc[-1]['sneeze mean'], 1)
    max_new_infections = max([new_infections, new_infections_compare])
    min_new_infections = min([new_infections, new_infections_compare])

    return "The number of infected is, on average, without a mask {} and with a mask {}, which is {} lower".format(max_new_infections, min_new_infections, round(max_new_infections/min_new_infections, 2))
    # return "Le nombre d'infectés en moyenne sans masque est de {} et avec masque de {}, ce qui est {} fois plus bas.".format(max_new_infections, min_new_infections, round(max_new_infections/min_new_infections, 2))


@app.callback(
    Output('titre5', 'children'),
    [Input('intermediate-value', 'children'),
     Input('compare_button', 'n_clicks'),
     Input('nb_simulation', 'value'),
     Input('duree_simu', 'value'),
     Input('nb_children', 'value'),
     Input('nb_children_inf', 'value'),
     Input('mask_use', 'on')])
def up_titre5(jsonified_cleaned_data, n_clicks, nb_simulation, duree_simu, nb_children, nb_children_inf, mask_use):
    if not n_clicks:
        raise PreventUpdate
    datasets = json.loads(jsonified_cleaned_data)
    origin_infection = pd.read_json(datasets['res_infected'], orient='split')
    origin_infection_compare = pd.read_json(
        datasets['res_infected_compare'], orient='split')

    rate_new_infections = (origin_infection.iloc[-1]['Touched own face mean'] +
                           origin_infection.iloc[-1]['sneeze mean'])/int(nb_children_inf)
    rate_new_infections_compare = (
        origin_infection_compare.iloc[-1]['Touched own face mean']+origin_infection_compare.iloc[-1]['sneeze mean'])/int(nb_children_inf)
    max_rate_new_infections = max(
        [rate_new_infections, rate_new_infections_compare])
    min_rate_new_infections = min(
        [rate_new_infections, rate_new_infections_compare])

    return ""
    # return "The resulted infection rates per person resulted from these simulations will be used below in our epidemic forecast models, respectively without mask {} and with mask {}".format(round(max_rate_new_infections, 1), round(min_rate_new_infections, 1))


@app.callback(
    Output('titre6', 'children'),
    [Input('intermediate-value', 'children'),
     Input('seir_model', 'n_clicks'),
     Input('nb_simulation', 'value'),
     Input('duree_simu', 'value'),
     Input('nb_children', 'value'),
     Input('nb_children_inf', 'value'),
     Input('mask_use', 'on')])
def up_titre6(jsonified_cleaned_data, n_clicks, nb_simulation, duree_simu, nb_children, nb_children_inf, mask_use):
    if not n_clicks:
        raise PreventUpdate
    datasets = json.loads(jsonified_cleaned_data)
    origin_infection = pd.read_json(datasets['res_infected'], orient='split')
    origin_infection_compare = pd.read_json(
        datasets['res_infected_compare'], orient='split')

    rate_new_infections = (origin_infection.iloc[-1]['Touched own face mean'] +
                           origin_infection.iloc[-1]['sneeze mean'])/int(nb_children_inf)
    rate_new_infections_compare = (
        origin_infection_compare.iloc[-1]['Touched own face mean']+origin_infection_compare.iloc[-1]['sneeze mean'])/int(nb_children_inf)
    max_rate_new_infections = max(
        [rate_new_infections, rate_new_infections_compare])
    min_rate_new_infections = min(
        [rate_new_infections, rate_new_infections_compare])

    return ""
    # return "Application du modèle SIR avec un taux de {} infections par jour".format(round(rate_new_infections, 2))
    # if rate_new_infections == max([rate_new_infections, rate_new_infections_compare]):
    #     return "SIR model without mask and infection rate per person {}".format(round(rate_new_infections, 2))
    # else:
    #     return "SIR model with mask and infection rate per person {}".format(round(rate_new_infections, 2))


@app.callback(
    Output('titre7', 'children'),
    [Input('intermediate-value', 'children'),
     Input('seir_model', 'n_clicks'),
     Input('nb_simulation', 'value'),
     Input('duree_simu', 'value'),
     Input('nb_children', 'value'),
     Input('nb_children_inf', 'value'),
     Input('mask_use', 'on')])
def up_titre7(jsonified_cleaned_data, n_clicks, nb_simulation, duree_simu, nb_children, nb_children_inf, mask_use):
    if not n_clicks:
        raise PreventUpdate
    datasets = json.loads(jsonified_cleaned_data)
    origin_infection = pd.read_json(datasets['res_infected'], orient='split')
    origin_infection_compare = pd.read_json(
        datasets['res_infected_compare'], orient='split')

    rate_new_infections = (origin_infection.iloc[-1]['Touched own face mean'] +
                           origin_infection.iloc[-1]['sneeze mean'])/int(nb_children_inf)
    rate_new_infections_compare = (
        origin_infection_compare.iloc[-1]['Touched own face mean']+origin_infection_compare.iloc[-1]['sneeze mean'])/int(nb_children_inf)
    max_rate_new_infections = max(
        [rate_new_infections, rate_new_infections_compare])
    min_rate_new_infections = min(
        [rate_new_infections, rate_new_infections_compare])

    return ""

    # return "Application du modèle SIR avec un taux de {} infections par jour".format(round(rate_new_infections_compare, 2))

    # if rate_new_infections_compare == min([rate_new_infections, rate_new_infections_compare]):
    #     return "SIR model with mask and infection rate per person {}".format(round(rate_new_infections_compare, 2))
    # else:
    #     return "SIR model without mask and infection rate per person {}".format(round(rate_new_infections_compare, 2))


@app.callback(
    Output('intermediate-value', 'children'),
    [Input('button', 'n_clicks'),
     Input('compare_button', 'n_clicks'),
     Input('nb_simulation', 'value'),
     Input('duree_simu', 'value'),
     Input('nb_children', 'value'),
     Input('nb_children_inf', 'value'),
     Input('mask_use', 'on')])
def run_script_onClick(n_clicks, n_clicks2, nb_simulation, duree_simu, nb_children, nb_children_inf, mask_use):
    if not n_clicks:
        raise PreventUpdate
    else:
        parameters = Parameters()
        # here make parameters vary from defaults
        parameters.n_humans["value"] = int(nb_children)
        parameters.n_infected_humans["value"] = int(nb_children_inf)  # Pareil
        parameters.mask["value"] = mask_use
        parameters.scenario["value"] = Scenario.EXIT_AFTER_DURATION
        parameters.scenario_duration["value"] = int(duree_simu)*60

        formated_parameters = shape_parameters_for_model_simulations(
            parameters)  # create **kwargs input for model init
        df_tot = pd.DataFrame()
        dict_tot = {InfectionOrigin.INITIALLY_INFECTED: [],
                    InfectionOrigin.TOUCHED_OWN_FACE: [], InfectionOrigin.SNEEZE: []}
        for i in range(0, int(nb_simulation)):
            res = run_model(formated_parameters)
            dict_tot[InfectionOrigin.INITIALLY_INFECTED].append(
                res.iloc[-1]["Infection Origins"][InfectionOrigin.INITIALLY_INFECTED])
            if InfectionOrigin.TOUCHED_OWN_FACE in res.iloc[-1]["Infection Origins"]:
                dict_tot[InfectionOrigin.TOUCHED_OWN_FACE].append(
                    res.iloc[-1]["Infection Origins"][InfectionOrigin.TOUCHED_OWN_FACE])

            if InfectionOrigin.SNEEZE in res.iloc[-1]["Infection Origins"]:
                dict_tot[InfectionOrigin.SNEEZE].append(
                    res.iloc[-1]["Infection Origins"][InfectionOrigin.SNEEZE])

            df_tot = df_tot.append(res)
        df_tot_statistics = df_tot
        res_infected = pd.DataFrame([statistics.median(
            dict_tot[InfectionOrigin.INITIALLY_INFECTED])], columns=['initially infected median'])
        res_infected['initially infected mean'] = statistics.mean(
            dict_tot[InfectionOrigin.INITIALLY_INFECTED])
        res_infected['initially infected std'] = statistics.stdev(
            dict_tot[InfectionOrigin.INITIALLY_INFECTED])

        if len(dict_tot[InfectionOrigin.SNEEZE]) == 0:
            True
        elif len(dict_tot[InfectionOrigin.SNEEZE]) == 1:
            res_infected['sneeze mean'] = statistics.mean(dict_tot[InfectionOrigin.SNEEZE])
            res_infected['sneeze median'] = statistics.median(
                dict_tot[InfectionOrigin.SNEEZE])
            res_infected['sneeze std'] = 0
        else:
            res_infected['sneeze mean'] = statistics.mean(dict_tot[InfectionOrigin.SNEEZE])
            res_infected['sneeze median'] = statistics.median(
                dict_tot[InfectionOrigin.SNEEZE])
            res_infected['sneeze std'] = statistics.stdev(dict_tot[InfectionOrigin.SNEEZE])

        if len(dict_tot[InfectionOrigin.TOUCHED_OWN_FACE]) == 0:
            True
        elif len(dict_tot[InfectionOrigin.TOUCHED_OWN_FACE]) == 1:
            res_infected['Touched own face mean'] = statistics.mean(
                dict_tot[InfectionOrigin.TOUCHED_OWN_FACE])
            res_infected['Touched own face median'] = statistics.median(
                dict_tot[InfectionOrigin.TOUCHED_OWN_FACE])
            res_infected['Touched own face std'] = 0
        else:
            res_infected['Touched own face mean'] = statistics.mean(
                dict_tot[InfectionOrigin.TOUCHED_OWN_FACE])
            res_infected['Touched own face median'] = statistics.median(
                dict_tot[InfectionOrigin.TOUCHED_OWN_FACE])
            res_infected['Touched own face std'] = statistics.stdev(
                dict_tot[InfectionOrigin.TOUCHED_OWN_FACE])
        df_tot_statistics_compare = pd.DataFrame()
        res_infected_compare = pd.DataFrame()
        datasets = {
            'df_tot_statistics': df_tot_statistics.to_json(orient='split', date_format='iso'),
            'res_infected': res_infected.to_json(orient='split', date_format='iso'),
            'df_tot_statistics_compare': df_tot_statistics_compare.to_json(orient='split', date_format='iso'),
            'res_infected_compare': res_infected_compare.to_json(orient='split', date_format='iso')
        }

        if n_clicks2:
            parameters = Parameters()
            # here make parameters vary from defaults
            parameters.n_humans["value"] = int(nb_children)
            parameters.n_infected_humans["value"] = int(nb_children_inf)
            if mask_use == True:
                parameters.mask["value"] = False
            else:
                parameters.mask["value"] = True

            parameters.scenario["value"] = Scenario.EXIT_AFTER_DURATION
            parameters.scenario_duration["value"] = int(duree_simu)*60

            # create **kwargs input for model init
            formated_parameters_compare = shape_parameters_for_model_simulations(parameters) 
             
            df_tot_compare = pd.DataFrame()
            dict_tot_compare = {InfectionOrigin.INITIALLY_INFECTED: [],
                                InfectionOrigin.TOUCHED_OWN_FACE: [], InfectionOrigin.SNEEZE: []}
            for i in range(0, int(nb_simulation)):
                res_compare = run_model(formated_parameters_compare)
                dict_tot_compare[InfectionOrigin.INITIALLY_INFECTED].append(
                    res_compare.iloc[-1]["Infection Origins"][InfectionOrigin.INITIALLY_INFECTED])
                if InfectionOrigin.TOUCHED_OWN_FACE in res_compare.iloc[-1]["Infection Origins"]:
                    dict_tot_compare[InfectionOrigin.TOUCHED_OWN_FACE].append(
                        res_compare.iloc[-1]["Infection Origins"][InfectionOrigin.TOUCHED_OWN_FACE])

                if InfectionOrigin.SNEEZE in res_compare.iloc[-1]["Infection Origins"]:
                    dict_tot_compare[InfectionOrigin.SNEEZE].append(
                        res_compare.iloc[-1]["Infection Origins"][InfectionOrigin.SNEEZE])

                df_tot_compare = df_tot_compare.append(res_compare)
            df_tot_statistics_compare = df_tot_compare
            res_infected_compare = pd.DataFrame([statistics.median(
                dict_tot_compare[InfectionOrigin.INITIALLY_INFECTED])], columns=['initially infected median'])
            res_infected_compare['initially infected mean'] = statistics.mean(
                dict_tot_compare[InfectionOrigin.INITIALLY_INFECTED])
            res_infected_compare['initially infected std'] = statistics.stdev(
                dict_tot_compare[InfectionOrigin.INITIALLY_INFECTED])

            if len(dict_tot_compare[InfectionOrigin.SNEEZE]) == 0:
                True
            elif len(dict_tot_compare[InfectionOrigin.SNEEZE]) == 1:
                res_infected_compare['sneeze mean'] = statistics.mean(
                    dict_tot_compare[InfectionOrigin.SNEEZE])
                res_infected_compare['sneeze median'] = statistics.median(
                    dict_tot_compare[InfectionOrigin.SNEEZE])
                res_infected_compare['sneeze std'] = 0
            else:
                res_infected_compare['sneeze mean'] = statistics.mean(
                    dict_tot_compare[InfectionOrigin.SNEEZE])
                res_infected_compare['sneeze median'] = statistics.median(
                    dict_tot_compare[InfectionOrigin.SNEEZE])
                res_infected_compare['sneeze std'] = statistics.stdev(
                    dict_tot_compare[InfectionOrigin.SNEEZE])

            if len(dict_tot_compare[InfectionOrigin.TOUCHED_OWN_FACE]) == 0:
                True
            elif len(dict_tot_compare[InfectionOrigin.TOUCHED_OWN_FACE]) == 1:
                res_infected_compare['Touched own face mean'] = statistics.mean(
                    dict_tot_compare[InfectionOrigin.TOUCHED_OWN_FACE])
                res_infected_compare['Touched own face median'] = statistics.median(
                    dict_tot_compare[InfectionOrigin.TOUCHED_OWN_FACE])
                res_infected_compare['Touched own face std'] = 0
            else:
                res_infected_compare['Touched own face mean'] = statistics.mean(
                    dict_tot_compare[InfectionOrigin.TOUCHED_OWN_FACE])
                res_infected_compare['Touched own face median'] = statistics.median(
                    dict_tot_compare[InfectionOrigin.TOUCHED_OWN_FACE])
                res_infected_compare['Touched own face std'] = statistics.stdev(
                    dict_tot_compare[InfectionOrigin.TOUCHED_OWN_FACE])
            datasets = {
                'df_tot_statistics': df_tot_statistics.to_json(orient='split', date_format='iso'),
                'res_infected': res_infected.to_json(orient='split', date_format='iso'),
                'df_tot_statistics_compare': df_tot_statistics_compare.to_json(orient='split', date_format='iso'),
                'res_infected_compare': res_infected_compare.to_json(orient='split', date_format='iso')
            }
        else:
            df_tot_statistics_compare = pd.DataFrame()
            res_infected_compare = pd.DataFrame()
            datasets = {
                'df_tot_statistics': df_tot_statistics.to_json(orient='split', date_format='iso'),
                'res_infected': res_infected.to_json(orient='split', date_format='iso'),
                'df_tot_statistics_compare': df_tot_statistics_compare.to_json(orient='split', date_format='iso'),
                'res_infected_compare': res_infected_compare.to_json(orient='split', date_format='iso')
            }

    return json.dumps(datasets)


@app.callback(Output('g1', 'figure'),
              [Input('button', 'n_clicks'),
               Input('intermediate-value', 'children'),
               Input('nb_simulation', 'value'),
               Input('duree_simu', 'value'),
               Input('nb_children', 'value'),
               Input('nb_children_inf', 'value'),
               Input('mask_use', 'on')])
def update_graph(n_clicks, jsonified_cleaned_data, nb_simulation, duree_simu, nb_children, nb_children_inf, mask_use):
    if not n_clicks:
        raise PreventUpdate

    datasets = json.loads(jsonified_cleaned_data)
    df_n = pd.read_json(datasets['df_tot_statistics'], orient='split')
    df_n.drop('Infection Origins', axis=1, inplace=True)
    resulttable_calc = df_n.groupby(level=0)
    df_stats = resulttable_calc.agg(['mean', 'std', 'median'])
    df_stats.columns = ['Infected Ratio mean', 'Infected Ratio std', 'Infected Ratio median',
                        'Hands Infected Ratio mean', 'Hands Infected Ratio std', 'Hands Infected Ratio median']
    df_stats.reindex(columns=sorted(df_stats.columns))
    dict_row = {'Infected Ratio mean': float(nb_children_inf)/float(nb_children), 'Infected Ratio std': 0,
                'Infected Ratio median': float(nb_children_inf)/float(nb_children), 'Hands Infected Ratio mean': 0,
                'Hands Infected Ratio std': 0, 'Hands Infected Ratio median': 0}
    row_df = pd.DataFrame(dict_row, index=[0])
    df_min = pd.concat([row_df, df_stats], ignore_index=True)
    df_minutes = df_min.loc[df_min.index % 60 == 0, :]
    traces = []

    for i, j in zip(['Infected Ratio mean', 'Hands Infected Ratio mean'], ['Infected Ratio std', 'Hands Infected Ratio std']):
        y1 = df_minutes[i].values
        x = (df_minutes.index.values)/60
        # std=float(df_minutes[j].values)/2
        traces.append(go.Scatter(
            x=x, y=y1,
            error_y=dict(
                type='data',  # value of error bar given in data coordinates
                array=df_minutes[j]/2,
                visible=True),
            # hovertemplate = '<i>Nb</i>: $%{y1:.2f}',#"'#':%{y1}{plusminus}{std}",
            line_color=['#c4350e', '#121ab0'][[
                'Infected Ratio mean', 'Hands Infected Ratio mean'].index(i)],
            name=['Infected Ratio', 'Hands Infected Ratio'][[
                'Infected Ratio mean', 'Hands Infected Ratio mean'].index(i)],
            # name=['Taux d\'infections', 'Taux d\infections par les mains][['Infected Ratio mean', 'Hands Infected Ratio mean'].index(i)],
        ))
    return {
        'data': traces,
        'layout': go.Layout(
            xaxis={'title': 'Time (min)'},
            #xaxis={'title': 'Temps (s)'},
            yaxis={'title': 'Infected ratio'},
            #yaxis={'title': 'Taux d\'infection'},
            legend=dict(x=-.1, y=1.2),
            margin=dict(l=15, r=10, t=10, b=30),
            height=400,  # px
            width=400
        )}


@app.callback(Output('g2', 'figure'),
              [Input('button', 'n_clicks'),
               Input('intermediate-value', 'children'),
               Input('nb_simulation', 'value'),
               Input('duree_simu', 'value'),
               Input('nb_children', 'value'),
               Input('nb_children_inf', 'value'),
               Input('mask_use', 'on')])
def update_graph2(n_clicks, jsonified_cleaned_data, nb_simulation, duree_simu, nb_children, nb_children_inf, mask_use):
    if not n_clicks:
        raise PreventUpdate
    datasets = json.loads(jsonified_cleaned_data)
    dff = pd.read_json(datasets['res_infected'], orient='split')
    traces = []
    traces.append(dict(
        x=[InfectionOrigin.INITIALLY_INFECTED, InfectionOrigin.TOUCHED_OWN_FACE, InfectionOrigin.SNEEZE],
        y=[float(dff['initially infected mean']), float(
            dff['Touched own face mean']), float(dff['sneeze mean'])],
        error_y=dict(type='data', array=[float(dff['initially infected std'])/2, float(
            dff['Touched own face std'])/2, float(dff['sneeze std'])/2]),
        type='bar'
    ))
    return {
        'data': traces,
        'layout': go.Layout(
            xaxis={'title': 'Origin of the infections'},
            #xaxis={'title': 'Origine d\'infection'},
            yaxis={'title': 'Number of infected'},
            #yaxis={'title': 'Nombre d\'infectés'},
            margin=dict(l=40, r=10, t=10, b=32),
            height=400,  # px
            width=400
        )}


@app.callback(Output('g1_compare', 'figure'),
              [Input('compare_button', 'n_clicks'),
               Input('intermediate-value', 'children'),
               Input('nb_simulation', 'value'),
               Input('duree_simu', 'value'),
               Input('nb_children', 'value'),
               Input('nb_children_inf', 'value'),
               Input('mask_use', 'on')])
def update_graph_compare1(n_clicks, jsonified_cleaned_data, nb_simulation, duree_simu, nb_children, nb_children_inf, mask_use):
    if not n_clicks:
        raise PreventUpdate
    datasets = json.loads(jsonified_cleaned_data)
    df_n = pd.read_json(datasets['df_tot_statistics'], orient='split')
    df_n.drop('Infection Origins', axis=1, inplace=True)
    resulttable_calc = df_n.groupby(level=0)
    df_stats = resulttable_calc.agg(['mean', 'std', 'median'])
    df_stats.columns = ['Infected Ratio mean', 'Infected Ratio std', 'Infected Ratio median',
                        'Hands Infected Ratio mean', 'Hands Infected Ratio std', 'Hands Infected Ratio median']
    df_stats.reindex(columns=sorted(df_stats.columns))
    dict_row = {'Infected Ratio mean': float(nb_children_inf)/float(nb_children), 'Infected Ratio std': 0,
                'Infected Ratio median': float(nb_children_inf)/float(nb_children), 'Hands Infected Ratio mean': 0,
                'Hands Infected Ratio std': 0, 'Hands Infected Ratio median': 0}
    row_df = pd.DataFrame(dict_row, index=[0])
    df_min = pd.concat([row_df, df_stats], ignore_index=True)
    df_minutes = df_min.loc[df_min.index % 60 == 0, :]

    # Simulation de comparaison
    df_n_compare = pd.read_json(
        datasets['df_tot_statistics_compare'], orient='split')
    df_n_compare.drop('Infection Origins', axis=1, inplace=True)
    resulttable_calc_compare = df_n_compare.groupby(level=0)
    df_stats_compare = resulttable_calc_compare.agg(['mean', 'std', 'median'])
    df_stats_compare.columns = ['Infected Ratio mean', 'Infected Ratio std', 'Infected Ratio median',
                                'Hands Infected Ratio mean', 'Hands Infected Ratio std', 'Hands Infected Ratio median']
    df_stats_compare.reindex(columns=sorted(df_stats.columns))
    dict_row_compare = {'Infected Ratio mean': float(nb_children_inf)/float(nb_children), 'Infected Ratio std': 0,
                        'Infected Ratio median': float(nb_children_inf)/float(nb_children), 'Hands Infected Ratio mean': 0,
                        'Hands Infected Ratio std': 0, 'Hands Infected Ratio median': 0}
    row_df_compare = pd.DataFrame(dict_row_compare, index=[0])
    df_min_compare = pd.concat(
        [row_df_compare, df_stats_compare], ignore_index=True)
    df_minutes_compare = df_min_compare.loc[df_min_compare.index % 60 == 0, :]

    traces = []
    for i, j in zip(['Infected Ratio mean', 'Hands Infected Ratio mean'], ['Infected Ratio std', 'Hands Infected Ratio std']):
        y1 = df_minutes[i].values
        x = (df_minutes.index.values)/60

        traces.append(go.Scatter(
            x=x, y=y1,
            error_y=dict(
                type='data',  # value of error bar given in data coordinates
                array=df_minutes[j]/2,
                visible=True),
            # hovertemplate = '<i>Nb</i>: $%{y1:.2f}',#"'#':%{y1}{plusminus}{std}",
            line_color=['#c4350e', '#121ab0'][[
                'Infected Ratio mean', 'Hands Infected Ratio mean'].index(i)],
            name=['Infected Ratio without mask', 'Hands Infected Ratio without mask'][[
                'Infected Ratio mean', 'Hands Infected Ratio mean'].index(i)],
            #name=['Taux d\'infections sans masque', 'Taux d\'infections par les mains, sans masque'][['Infected Ratio mean', 'Hands Infected Ratio mean'].index(i)],
        ))

    for i, j in zip(['Infected Ratio mean', 'Hands Infected Ratio mean'], ['Infected Ratio std', 'Hands Infected Ratio std']):
        y1_compare = df_minutes_compare[i].values
        std_compare = df_minutes_compare[j]/2
        x = (df_minutes.index.values)/60

        traces.append(go.Scatter(
            x=x, y=y1_compare,
            error_y=dict(
                type='data',  # value of error bar given in data coordinates
                array=df_minutes_compare[j]/2,
                visible=True),
            # hovertemplate = '<i>Nb</i>: $%{y1:.2f}',#"'#':%{y1}{plusminus}{std}",
            line_color=['#eb7617', '#a613cf'][[
                'Infected Ratio mean', 'Hands Infected Ratio mean'].index(i)],
            name=['Infected Ratio with mask', 'Hands Infected Ratio with mask'][[
                'Infected Ratio mean', 'Hands Infected Ratio mean'].index(i)],
            #name=['Taux d\'infections avec masque', 'Taux d\'infections par les mains, avec masque'][['Infected Ratio mean', 'Hands Infected Ratio mean'].index(i)],
        ))

    return {
        'data': traces,
        'layout': go.Layout(
            xaxis={'title': 'Time (min)'},
            #xaxis={'title': 'Temps (s)'},
            yaxis={'title': 'Infected ratio'},
            #yaxis={'title': 'Taux d\'infection'},
            legend=dict(x=-.1, y=1.2),
            margin=dict(l=45, r=10, t=2, b=30),
            height=400,  # px
            width=400
        )}


@app.callback(Output('g2_compare', 'figure'),
              [Input('compare_button', 'n_clicks'),
               Input('intermediate-value', 'children'),
               Input('nb_simulation', 'value'),
               Input('duree_simu', 'value'),
               Input('nb_children', 'value'),
               Input('nb_children_inf', 'value'),
               Input('mask_use', 'on')])
def update_graph_compare2(n_clicks, jsonified_cleaned_data, nb_simulation, duree_simu, nb_children, nb_children_inf, mask_use):
    if not n_clicks:
        raise PreventUpdate
    datasets = json.loads(jsonified_cleaned_data)
    dff = pd.read_json(datasets['res_infected'], orient='split')
    dff_compare = pd.read_json(
        datasets['res_infected_compare'], orient='split')

    traces = []
    traces.append(dict(
        name='Without mask',
        x=[InfectionOrigin.INITIALLY_INFECTED, InfectionOrigin.TOUCHED_OWN_FACE, InfectionOrigin.SNEEZE],
        y=[float(dff['initially infected mean']), float(
            dff['Touched own face mean']), float(dff['sneeze mean'])],
        #y=[int(dff['initially infected mean']),int(dff['Touched own face mean']),int(dff['sneeze mean'])],
        error_y=dict(type='data', array=[float(dff['initially infected std'])/2, float(
            dff['Touched own face std'])/2, float(dff['sneeze std'])/2]),
        type='bar'
    ))
    traces.append(dict(
        name='With mask',
        x=[InfectionOrigin.INITIALLY_INFECTED, InfectionOrigin.TOUCHED_OWN_FACE, InfectionOrigin.SNEEZE],
        y=[float(dff_compare['initially infected mean']), float(
            dff_compare['Touched own face mean']), float(dff_compare['sneeze mean'])],
        #y=[int(dff_compare['initially infected mean']),int(dff_compare['Touched own face mean']),int(dff_compare['sneeze mean'])],
        error_y=dict(type='data', array=[float(dff_compare['initially infected std'])/2, float(
            dff_compare['Touched own face std'])/2, float(dff_compare['sneeze std'])/2]),
        type='bar'
    ))
    return {
        'data': traces,
        'layout': go.Layout(
            xaxis={'title': 'Origin of the infections'},
            #xaxis={'title': 'Origine d\'infection'},
            yaxis={'title': 'Number of infected'},
            #yaxis={'title': 'Nombre d\'infectés'},
            #margin=dict(l=40, r=10, t=10, b=32),
            legend=dict(x=-.1, y=1.2),
            margin=dict(l=20, r=10, t=2, b=30),
            height=400,  # px
            width=400
        )}


@app.callback(
    Output('g1_seir', 'figure'),
    [Input('seir_model', 'n_clicks'),
     Input('intermediate-value', 'children'),
     Input('nb_simulation', 'value'),
     Input('duree_simu', 'value'),
     Input('nb_children', 'value'),
     Input('nb_children_inf', 'value'),
     Input('mask_use', 'on')])
def update_graph_seir1(n_clicks, jsonified_cleaned_data, nb_simulation, duree_simu, nb_children, nb_children_inf, mask_use):
    if not n_clicks:
        raise PreventUpdate
    datasets = json.loads(jsonified_cleaned_data)
    resultat = pd.read_json(datasets['res_infected'], orient='split')
    Total_valeur = float(resultat['initially infected mean'].values)+float(
        resultat['Touched own face mean'].values)+float(resultat['sneeze mean'].values)
    #Total_valeur=int(resultat['initially infected mean'].values)+int(resultat['Touched own face mean'].values)+int(resultat['sneeze mean'].values)
    beta = float((Total_valeur-float(resultat['initially infected mean'].values))/float(
        resultat['initially infected mean'].values)) / 4
    #beta=int((Total_valeur-int(resultat['initially infected mean'].values))/int(resultat['initially infected mean'].values))
    kappa = 0.3  # 30% of the infected will go on quanratine
    delta = 1/14.0  # They stay in quarantine during 14 days

    nb_classe = 1000
    N = nb_classe*int(nb_children)
    D = 7.0  # infections lasts four days
    alpha = 1.0 / 2.5 #incubation last 2.5 days
    gamma = 1.0 / D
    nu = 0.000001  # 0.0001 death rate
    rho = 1/9  # 9 days from infection until death
    
    # 1000 classes de 30 élèves
    nb_children_inf_tot = int(nb_children_inf)*0.1*nb_classe
    # initial conditions: one exposed, rest susceptible
    S0, E0, I0, Q0, R0, D0 = N-nb_children_inf_tot,0, nb_children_inf_tot, 0., 0., 0.

    # beta = R_0*gamma  # infected person infects 1 other person per day
    # t = np.linspace(0, 100, 100) # Grid of time points (in days)
    t = np.linspace(0, 50, 50)  # Grid of time points (in days)
    y0 = S0, E0, I0, Q0, R0, D0  # Initial conditions vector

    ret = integration_equations(deriv, y0, t, N, beta, gamma, kappa, delta,alpha,nu,rho)
    S, E, I, Q, R, D = ret.T
    traces = []
    traces.append(go.Scatter(
        x=t,
        y=S,
        name='Susceptible',
        marker_color='blue'
    ))
    traces.append(go.Scatter(
        x=t,
        y=E,
        name='Exposed',
        marker_color='orange'

    ))
    traces.append(go.Scatter(
        x=t,
        y=I,
        name='Infected',
        marker_color='red'

    ))
    traces.append(go.Scatter(
        x=t,
        y=Q,
        name='Quarantined',
        marker_color='indigo'

    ))
    traces.append(go.Scatter(
        x=t,
        y=R,
        name='Recovered',
        marker_color='green'

    ))
    traces.append(go.Scatter(
        x=t,
        y=D,
        name='Death',
        marker_color='black'

    ))
    return {
        'data': traces,
        'layout': go.Layout(
            xaxis={'title': 'Time (days)'},
            #xaxis={'title': 'Temps (jours)'},
            yaxis={'title': 'Number'},
            #yaxis={'title': 'Nombre'},
            legend=dict(x=-.1, y=1.4),
            #legend=dict(x=-.1, y=1.2),
            margin=dict(l=20, r=10, t=1, b=30),
            height=400,  # px
            width=400
        )}


@app.callback(
    Output('g2_seir', 'figure'),
    [Input('seir_model', 'n_clicks'),
     Input('intermediate-value', 'children'),
     Input('nb_simulation', 'value'),
     Input('duree_simu', 'value'),
     Input('nb_children', 'value'),
     Input('nb_children_inf', 'value'),
     Input('mask_use', 'on')])
def update_graph_seir2(n_clicks, jsonified_cleaned_data, nb_simulation, duree_simu, nb_children, nb_children_inf, mask_use):
    if not n_clicks:
        raise PreventUpdate
    datasets = json.loads(jsonified_cleaned_data)
    resultat = pd.read_json(datasets['res_infected_compare'], orient='split')
    Total_valeur = float(resultat['initially infected mean'].values)+float(
        resultat['Touched own face mean'].values)+float(resultat['sneeze mean'].values)
    #Total_valeur=int(resultat['initially infected mean'].values)+int(resultat['Touched own face mean'].values)+int(resultat['sneeze mean'].values)
    #beta=int((Total_valeur-int(resultat['initially infected mean'].values))/int(resultat['initially infected mean'].values))
    beta = float((Total_valeur-float(resultat['initially infected mean'].values))/float(
        resultat['initially infected mean'].values)) / 4
    kappa = 0.3  # 30% of the infected will go on quanratine
    delta = 1/14.0  # They stay in quarantine during 14 days

    nb_classe = 1000
    N = nb_classe*int(nb_children)
    D = 7.0  # infections lasts four days
    alpha = 1.0 / 2.5 #incubation last 2.5 days
    gamma = 1.0 / D
    nu = 0.000001  # 0.0001 death rate
    rho = 1/9  # 9 days from infection until death
    
    # 1000 classes de 30 élèves
    nb_children_inf_tot = int(nb_children_inf)*0.1*nb_classe
    # initial conditions: one exposed, rest susceptible
    S0, E0, I0, Q0, R0, D0 = N-nb_children_inf_tot,0, nb_children_inf_tot, 0., 0., 0.

    # beta = R_0*gamma  # infected person infects 1 other person per day
    # t = np.linspace(0, 100, 100) # Grid of time points (in days)
    t = np.linspace(0, 50, 50)  # Grid of time points (in days)
    y0 = S0, E0, I0, Q0, R0, D0  # Initial conditions vector

    ret = integration_equations(deriv, y0, t, N, beta, gamma, kappa, delta,alpha,nu,rho)
    S, E, I, Q, R, D = ret.T
    traces = []
    traces.append(go.Scatter(
        x=t,
        y=S,
        name='Susceptible',
        marker_color='blue'
    ))
    traces.append(go.Scatter(
        x=t,
        y=E,
        name='Exposed',
        marker_color='orange'

    ))
    traces.append(go.Scatter(
        x=t,
        y=I,
        name='Infected',
        marker_color='red'

    ))
    traces.append(go.Scatter(
        x=t,
        y=Q,
        name='Quarantined',
        marker_color='indigo'

    ))
    traces.append(go.Scatter(
        x=t,
        y=R,
        name='Recovered',
        marker_color='green'

    ))
    traces.append(go.Scatter(
        x=t,
        y=D,
        name='Death',
        marker_color='black'

    ))
    return{
        'data': traces,
        'layout': go.Layout(
            xaxis={'title': 'Time (days)'},
            #xaxis={'title': 'Temps (jours)'},
            yaxis={'title': 'Number'},
            #yaxis={'title': 'Nombre'},
            #Intervalle [-2, 3]
            legend=dict(x=-.1, y=1.4),
            #legend=dict(x=-.1, y=1.2),
            margin=dict(l=20, r=10, t=1, b=30),
            height=400,  # px
            width=400
        )}


if __name__ == '__main__':
    app.run_server(debug=True, port=8053)
