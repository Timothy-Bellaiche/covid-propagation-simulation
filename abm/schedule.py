import random
from collections import defaultdict

from mesa.time import BaseScheduler

class HumansThenViruses(BaseScheduler):
    '''
    A scheduler which activates each type of agent once per step, in random
    order, with the order reshuffled every step.

    This is equivalent to the NetLogo 'ask breed...' and is generally the
    default behavior for an ABM.

    Assumes that all agents have a step() method.
    '''
    agents_by_type = defaultdict(list)

    def __init__(self, model):
        super().__init__(model)
        self.agents_by_type = defaultdict(list)


    def add(self, agent):
        '''
        Add an Agent object to the schedule

        Args:
            agent: An Agent to be added to the schedule.
        '''

        super().add(agent)
        agent_type = agent.agent_type
        self.agents_by_type[agent_type].append(agent)

    def remove(self, agent):
        '''
        Remove all instances of a given agent from the schedule.
        '''

        super().remove(agent)

        agent_type = agent.agent_type
        while agent in self.agents_by_type[agent_type]:
            self.agents_by_type[agent_type].remove(agent)

    def step(self):
        '''
        Executes the step of each human then of each virus

        '''
        
        humans = self.agents_by_type["human"]
        for human in humans:
            human.step()
        
        viruses = self.agents_by_type["virus"]
        for virus in viruses:
            virus.step()

        self.steps += 1
        self.time += 1

      
    def get_humans_by_infection(self, infected):
        '''
        Returns a list of agents based on the value of the infected parameter
        '''
        filtered_agents = [agent for agent in self.agents_by_type["human"] if agent.infected == infected]
        return filtered_agents

    def get_infected_ratio(self):
        '''
        Returns the current ratio of agents that are infected.
        '''
        return len(self.get_humans_by_infection(True)) / len(self.agents_by_type["human"])

    def get_hands_infected_ratio(self):
        hands_infected_agents = [agent for agent in self.agents_by_type["human"] if agent.hands_infected == True]
        return len(hands_infected_agents) / len(self.agents_by_type["human"])
    
    def get_infection_origins(self):
        '''
        Returns a dict of people infected by origin
        '''
        infection_origins = {}
        for human in self.agents_by_type["human"]:
            if human.infected == True:
                if human.infected_origin in infection_origins:
                    infection_origins[human.infected_origin] += 1
                else:
                    infection_origins[human.infected_origin] = 1
        
        return infection_origins

    def update_humans_movement_type(self, movement_type):
        '''
        Changes the movement behavior of human angents
        '''
        for human in self.agents_by_type["human"]:
            human.set_exit_as_goal()
            human.movement_type = movement_type

    def humans_still_rendered(self):
        for human in self.agents_by_type["human"]:
            if human.render == True:
                return True
        
        return False



