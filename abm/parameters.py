'''
WARRNING : You need to add every parameter name in
'''
import types

from mesa.visualization.UserParam import UserSettableParameter
from abm.movement.behavior import MovementType, SpawnType
from abm.model import Scenario


class Parameters():
    def __init__(self):
        # Examples:

        # # Simple number input
        # number_option = UserSettableParameter('number', 'My Number', value=123)

        # # Checkbox input
        # boolean_option = UserSettableParameter('checkbox', 'My Boolean', value=True)

        # # Choice input
        # choice_option = UserSettableParameter('choice', 'My Choice', value='Default choice',
        #                                       choices=['Default Choice', 'Alternate Choice'])

        # # Slider input
        # slider_option = UserSettableParameter('slider', 'My Slider', value=123, min_value=10, max_value=200, step=0.1)

        # # Static text
        # static_text = UserSettableParameter('static_text', value="This is a descriptive textbox")
        self.scenario = {
            "description": "Scenario",
            "type": "choice",
            "value": Scenario.DEFAULT,
            "choices": [
                Scenario.DEFAULT,
                Scenario.EXIT_AFTER_DURATION,
                Scenario.STOP_AFTER_DURATION
            ]
        }

        self.scenario_duration = {
            "description": "Scenario duration (s)",
            "type": "number",
            "value": 600
        }

        self.movement_type = {
            "description": "Movement behaviour",
            "type": "choice",
            "value": MovementType.RANDOM,
            "choices": [
                MovementType.RANDOM,
                MovementType.EXIT
            ]
        }

        self.spawn_type = {
            "description": "Initial spawning positions",
            "type": "choice",
            "value": SpawnType.RANDOM,
            "choices": [
                SpawnType.RANDOM
            ]
        }

        self.n_humans = {
            "description": "Number of employees",
            "value": 20,
            "type": "number",
        }

        self.n_infected_humans = {
            "description": "Number of initially infected employees",
            "value": 1,
            "type": "number",
        }

        self.mask = {
            "description": "Employees wear a mask",
            "value": False,
            "type": "checkbox",
        }

        self.sneeze_probability = {
            "description": "Sneeze probability",
            "value": 0.0075,
            "min_value": 0,
            "max_value": 0.1,
            "type": "slider",
            "step": 0.0005
        }

        self.sneeze_radius_without_mask = {
            "description": "Sneeze radius without mask",
            "value": 1.5,
            "min_value": 0,
            "max_value": 5,
            "type": "slider",
            "step": 0.25
        }

        self.sneeze_radius_with_mask = {
            "description": "Sneeze radius with a mask",
            "value": 0.5,
            "min_value": 0,
            "max_value": 5,
            "type": "slider",
            "step": 0.25
        }

        self.move_probability = {
            "description": "Probability to decide to move",
            "value": 0.025,
            "min_value": 0,
            "max_value": 1,
            "type": "slider",
            "step": 0.005
        }

        self.move_velocity = {
            "description": "Movement velocity",
            "value": 0.25,
            "min_value": 0,
            "max_value": 2,
            "type": "slider",
            "step": 0.05
        }

        self.next_node_offset = {
            "description": "Pathfinding node validation offset",
            "value": 0.1,
            "min_value": 0,
            "max_value": 1,
            "type": "slider",
            "step": 0.025
        }

        self.touch_face_probability = {
            "description": "Probability to touch own face",
            "value": 0.002,
            "min_value": 0,
            "max_value": 0.1,
            "type": "slider",
            "step": 0.001
        }

        self.touch_other_hands_probability = {
            "description": "Probability to touch another one's hands when close",
            "value": 0.01,
            "min_value": 0,
            "max_value": 1,
            "type": "slider",
            "step": 0.01
        }

        self.virus_air_duration = {
            "description": "Time the virus stays in the air (s)",
            "value": 10,
            "min_value": 1,
            "max_value": 100,
            "type": "slider",
            "step": 1
        }

        self.infection_probability = {
            "description": "Probability to inhale the virus and get infected",
            "value": 0.15,
            "min_value": 0,
            "max_value": 1,
            "type": "slider",
            "step": 0.01
        }

        self.hands_infected_probability = {
            "description": "Probability that the virus in the air infect hands",
            "value": 0.45,
            "min_value": 0,
            "max_value": 1,
            "type": "slider",
            "step": 0.01
        }



def shape_parameters_for_visualization():
    parameters = Parameters()
    new_params = {}
    for name, param in parameters.__dict__.items():
        if param["type"] == "slider":
            new_param = UserSettableParameter('slider', param["description"], value=param["value"], min_value=param["min_value"], max_value=param["max_value"], step=param["step"])
        elif param["type"] == "number":
            new_param = UserSettableParameter('number', param["description"], value=param["value"])
        elif param["type"] == "checkbox":
            new_param = UserSettableParameter('checkbox', param["description"], value=param["value"])
        elif param["type"] == "choice":
            new_param = UserSettableParameter('choice', param["description"], value=param["value"], choices=param["choices"])
        else:
            # TODO - other parameters types
            pass
        
        new_params[name] = new_param
    
    return new_params
        

def shape_parameters_for_model_simulations(parameters):
    parameters = parameters.__dict__.items()
    new_params = {}
    for name, param in parameters:
        new_params[name] = param["value"]
    
    return new_params

# TODO - See if we require more interactive data that doesn't require the simulation to be restarted
# Used to transform the data in a shape usable by javascript
def parameters_list(params):
    parameters = []
    for key, value in params.__dict__.items():
        param = types.SimpleNamespace()
        param.name = key
        param.value = value
        parameters.append(param)
    return parameters


# Used by the front to update the model parameters
def update_params(name, value):
    setattr(parameters, name, value)
    return parameters
