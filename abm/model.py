import random
import numpy as np

from scipy.spatial import Delaunay

from mesa import Model
from mesa.datacollection import DataCollector

from abm.agents import Human, Virus
from abm.schedule import HumansThenViruses
from abm.movement.navmesh import Navmesh
from abm.environments.shapes import complex_shape
from abm.movement.behavior import SpawnType, MovementType
from abm.movement.space import ContinuousSpace

class Scenario():
    DEFAULT = "Default never ending scenario"
    STOP_AFTER_DURATION = "Behavior stops after duration"
    EXIT_AFTER_DURATION = "Exit after duration"

class CovidContagion(Model):
    '''
    COVID SPREAD SIMULATION
    '''

    def __init__(self, **kwargs):
        '''
        Model
        '''        
        
        # TODO TEMP shape
        shape = complex_shape()

        # TODO END TEMP shape

        for key, value in kwargs.items():
            setattr(self, key, value)

        self.schedule = HumansThenViruses(self)
        self.grid = ContinuousSpace(shape["width"], shape["height"], False)
        self.navmesh = Navmesh(shape)
        self.scenario_finished = False
        
        self.datacollector = DataCollector(
            {"Infected Ratio": lambda m: m.schedule.get_infected_ratio(),
             "Hands Infected Ratio": lambda m: m.schedule.get_hands_infected_ratio(),
             "Infection Origins": lambda m: m.schedule.get_infection_origins()})

        # Create humans:
        for i in range(self.n_humans):

            if self.spawn_type == SpawnType.RANDOM:
                # first spawn
                pos = self.navmesh.get_random_node()
                # for additionnal spanws, check that there is nobody in a 1m radius
                while i != 0 and len(self.grid.get_neighbors(pos, radius=1)) > 0:
                    pos = self.navmesh.get_random_node()
                    break

            # define if initially infected
            infected = False
            if i < self.n_infected_humans:
                infected = True

            # place agent
            human = Human(pos, self, infected, self.movement_type)
            self.grid.place_agent(human, pos)
            self.schedule.add(human)

        self.running = True

    def step(self):
        # if the scenario is over, do nothing
        if self.scenario_finished == True:
            return

        # Default scenario - never ends
        if self.scenario == Scenario.DEFAULT:
            self.schedule.step()
            self.datacollector.collect(self)

        # Move for a time then stop
        if self.scenario == Scenario.STOP_AFTER_DURATION:
            # Check if duration is finished
            if self.schedule.time <=  self.scenario_duration:
                self.schedule.step()
                self.datacollector.collect(self)
            else:
                self.scenario_finished = True
            

        # Move for a time then exit
        if self.scenario == Scenario.EXIT_AFTER_DURATION:
            
            # Check if duration is finished
            if self.schedule.time <  self.scenario_duration:
                self.schedule.step()
                self.datacollector.collect(self)


            # if duration is over
            else:
                # at the moment the time is over, make them move to exit
                if self.schedule.time == self.scenario_duration:
                    self.schedule.update_humans_movement_type(MovementType.EXIT)
                
                # check if there are still agents that are present on the map
                if self.schedule.humans_still_rendered():
                    self.schedule.step()
                    self.datacollector.collect(self)
                
                else:
                    self.scenario_finished = True
            


    def run_model(self, step_count=200):
        if self.scenario == Scenario.DEFAULT or self.scenario == Scenario.STOP_AFTER_DURATION:
            for i in range(step_count):
                self.step()
        
        if self.scenario == Scenario.EXIT_AFTER_DURATION:
            while self.scenario_finished == False:
                self.step()        
