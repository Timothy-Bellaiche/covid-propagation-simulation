import random
import math
import uuid

from abm.movement.behavior import MovementType, MovingAgent

from mesa import Agent

class InfectionOrigin():
    NOT_INFECTEd = "not infected"
    INITIALLY_INFECTED = "initially infected"
    SNEEZE = "sneeze"
    TOUCHED_OWN_FACE = "touched own face"

class Human(MovingAgent):
    '''
    A human agent
    '''

    def __init__(self, pos, model, infected, movement_type):
        super().__init__(pos, uuid.uuid1().int, model, movement_type)
        self.pos = pos
        self.agent_type = "human"
        self.infected = infected
        self.infected_origin = InfectionOrigin.NOT_INFECTEd
        if infected == True:
            self.infected_origin = InfectionOrigin.INITIALLY_INFECTED
        
        self.hands_infected = False
        self.render = True


    def step(self):
        """
        A model step
        """
        # if the agent is out, don't compute his behaviour
        if not self.render:
            return
        
        # Sneeze check
        if self.infected == True and self.infected_origin == InfectionOrigin.INITIALLY_INFECTED:
            sneeze_check = self.random.random()
            if sneeze_check < self.model.sneeze_probability:
                radius = self.model.sneeze_radius_without_mask
                if self.model.mask:
                    radius = self.model.sneeze_radius_with_mask
                virus = Virus(self.pos, self.model, origin=InfectionOrigin.SNEEZE, duration=self.model.virus_air_duration, radius=radius)
                self.model.grid.place_agent(virus, self.pos)
                self.model.schedule.add(virus)
                return
                # print("Agent sneezed in pos {}".format(self.pos))
        
        # Touch agents around
        if self.hands_infected:
            for agent in self.model.grid.get_neighbors(self.pos, radius=0.5):
                if agent.agent_type == "human":
                    touch_hands_check = self.random.random()
                    if touch_hands_check < self.model.touch_other_hands_probability:
                        agent.hands_infected = True
                        return
                    # print("Agent infected other human's hands in pos {}".format(self.pos))
            
        # Touch own face check    
        touch_face_check = self.random.random()
        if touch_face_check < self.model.touch_face_probability:
            if self.infected == False and self.hands_infected == True:
                self.infected = True
                self.infected_origin = InfectionOrigin.TOUCHED_OWN_FACE
                # print("Agent infected himself by touching his face in pos {}".format(self.pos))

                
            if self.infected == True:
                self.hands_infected = True
                # print("Agent infected his hands by touching his face in pos {}".format(self.pos))

        # Movement handler
        self.move_step()


class Virus(Agent):
    '''
    A virus agent
    '''
    def __init__(self, pos, model, origin, duration, radius):
        super().__init__(uuid.uuid1().int, model)
        self.agent_type = "virus"
        self.duration = duration
        self.agent = Virus
        self.origin = origin
        self.radius = radius
        self.render = True

    def step(self):
        agents = self.model.grid.get_neighbors(self.pos, radius=self.radius)
        for agent in agents:
            if agent.agent_type == "human":

                if agent.infected == False:
                    # infect human check
                    infection_check = self.random.random()
                    if infection_check < self.model.infection_probability:
                        agent.infected = True
                        agent.infected_origin = self.origin

                # infect hands check
                hands_infection_check = self.random.random()
                if hands_infection_check < self.model.hands_infected_probability:
                    agent.hands_infected = True
                
        self.duration -= 1
        if self.duration <= 0:
            self.model.schedule.remove(self)
            self.model.grid.remove_agent(self)