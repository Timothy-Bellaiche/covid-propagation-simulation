from scipy.spatial import distance
import triangle as tr
import numpy as np

import matplotlib.pyplot as plt
import matplotlib.path as mpltPath

import math
import helpers.helpers as h

import random


class Navmesh():
    def __init__(self, shape, options='pqa0.75'):

        
        # Select only the 3 parts of the shape that are useful
        self.shape = shape
        formated_shape = {
            "vertices": shape["vertices"],
            "segments": shape["segments"],
            "holes": shape["holes"]
        }
        self.triangles = tr.triangulate(formated_shape, options)

        self.graph = Graph()

        for triangle in self.triangles["triangles"]:
            for A in self.triangles["vertices"][triangle]:
                for B in self.triangles["vertices"][triangle]:
                    if np.array_equal(A, B):
                        continue
                    self.graph.connect(A, B, h.euclidean_distance(A, B))

        # Todo, get height and width dynamically
        self.height = 10
        self.width = 10

    def plot_navmesh(self):
        tr.compare(plt, self.shape, self.triangles)
        plt.show()

    def plot_path(self, path):
        path_points = np.array(path)

        tr.plot(plt.axes(), **self.triangles)
        plt.scatter(path_points[-1, 0], path_points[-1, 1], marker='x', color="g", s=400)
        plt.scatter(path_points[0, 0], path_points[0, 1], marker='x', color="g", s=400)
        plt.scatter(path_points[1:-1, 0], path_points[1:-1, 1], marker='x', color="b", s=400)
        plt.show()

    # A* search
    def astar_search(self, start, end):
        final_path = []
        first_path_step = None
        last_path_step = None
        # Check if start and end are on the graph
        # if not, add a first and last step to make the agent go to / leave the graph
        if start not in self.graph.graph_dict:
            astar_start, distance = self.get_closest_node(start, end)
            # first_path_step = str(start) + ':' + str(distance)
            first_path_step = start
            start = astar_start

        if end not in self.graph.graph_dict:
            astar_end, distance = self.get_closest_node(end, start)
            # last_path_step = str(end) + ':' + str(distance)
            last_path_step = end
            end = astar_end


        # Create lists for open nodes and closed nodes
        open = []
        closed = []

        # Create a start node and an goal node
        start_node = Node(start, None)
        goal_node = Node(end, None)

        # Add the start node
        open.append(start_node)

        # Loop until the open list is empty
        while len(open) > 0:

            # Sort the open list to get the node with the lowest cost first
            open.sort()

            # Get the node with the lowest cost
            current_node = open.pop(0)

            # Add the current node to the closed list
            closed.append(current_node)

            # Check if we have reached the goal, return the path
            # if current_node == goal_node:
            if np.array_equal(current_node, goal_node):
                path = []
                while current_node != start_node:
                    # path.append(str(current_node.name) + ': ' + str(current_node.g))
                    path.append(current_node.name)
                    current_node = current_node.parent
                # path.append(str(start_node.name) + ': ' + str(start_node.g))
                path.append(start_node.name)
                
                # Return first element, reversed path and last element
                if first_path_step:
                    final_path.append(first_path_step)
                
                final_path.extend(path[::-1])
                
                if last_path_step:
                    final_path.append(last_path_step)

                return final_path

            # Get neighbours
            neighbors = self.graph.get(current_node.name)

            # Loop neighbors
            for key, value in neighbors.items():

                # Create a neighbor node
                neighbor = Node(key, current_node)

                # Check if the neighbor is in the closed list
                if(neighbor in closed):
                    continue

                # Calculate full path cost
                neighbor.g = current_node.g + \
                    self.graph.get(current_node.name, neighbor.name)
                neighbor.h = h.manhattan_distance(key, goal_node.name)
                neighbor.f = neighbor.g + neighbor.h

                # Check if neighbor is in open list and if it has a lower f value
                if(add_to_open(open, neighbor) == True):
                    # Everything is green, add neighbor to open list
                    open.append(neighbor)

        # Return empty final path, no path is found
        return final_path

    def get_closest_node(self, position, target_position):

        # simplex = self.delaunay.find_simplex(position)
        # points = self.delaunay.points[self.delaunay.simplices[simplex]]
        
        # finding the 5 closest points by sorting by distance
        closest_points_ids = distance.cdist([position], self.triangles["vertices"]).argsort()[0, :5]

        # get the 5 positions
        closest_points = self.triangles["vertices"][closest_points_ids]

        closest_distance = None
        closest_point = None
        for point in closest_points:
            dist = h.manhattan_distance(point, target_position)
            if not closest_distance or dist < closest_distance:
                closest_point = point
                closest_distance = dist
        
        # the return distance should be between the start and the astar_start
        dist = h.euclidean_distance(position, closest_point)

        # return a tuple for position
        closest_point = tuple(closest_point)
        return closest_point, dist

    def get_random_node(self):
        random_vertice = random.randint(0, len(self.triangles["vertices"]) - 1)

        return tuple(self.triangles["vertices"][random_vertice])

# This class represent a graph
class Graph:

    # Initialize the class
    def __init__(self, graph_dict={}, directed=False):
        self.graph_dict = graph_dict
        self.directed = directed
        if not directed:
            self.make_undirected()

    # Create an undirected graph by adding symmetric edges
    def make_undirected(self):
        for a in list(self.graph_dict.keys()):
            for (b, dist) in self.graph_dict[a].items():
                self.graph_dict.setdefault(b, {})[a] = dist

    # Add a link from A and B of given distance, and also add the inverse link if the graph is undirected
    def connect(self, A, B, distance):
        A = tuple(A)
        B = tuple(B)
        self.graph_dict.setdefault(A, {})[B] = distance
        if not self.directed:
            self.graph_dict.setdefault(B, {})[A] = distance

    # Get neighbors or a neighbor
    def get(self, a, b=None):
        links = self.graph_dict.setdefault(a, {})
        if b is None:
            return links
        else:
            return links.get(b)

    # Return a list of nodes in the graph
    def nodes(self):
        s1 = set([k for k in self.graph_dict.keys()])
        s2 = set([k2 for v in self.graph_dict.values()
                  for k2, v2 in v.items()])
        nodes = s1.union(s2)
        return list(nodes)




# This class represent a node
class Node:

    # Initialize the class
    def __init__(self, name: tuple, parent: tuple):
        self.name = name
        self.parent = parent
        self.g = 0  # Distance to start node
        self.h = 0  # Distance to goal node
        self.f = 0  # Total cost

    # Compare nodes
    def __eq__(self, other):
        return self.name == other.name

    # Sort nodes
    def __lt__(self, other):
        return self.f < other.f

    # Print node
    def __repr__(self):
        # return ('({0},{1})'.format(self.position, self.f))
        return str(self.name)


# Check if a neighbor should be added to open list
def add_to_open(open, neighbor):
    for node in open:
        if (neighbor == node and neighbor.f > node.f):
            return False
    return True

