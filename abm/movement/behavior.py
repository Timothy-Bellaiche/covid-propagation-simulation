from mesa import Agent
from enum import Enum
import numpy as np
import json

import helpers.helpers as h

class MovementType():
    RANDOM = "Random"
    EXIT = "Exit environment"

class SpawnType():
    RANDOM = "Random spawn"


class MovingAgent(Agent):
    def __init__(self, pos, id, model, movement_type):
        super().__init__(id, model)
        self.pos = pos
        self.movement_type = self.model.movement_type
        self.next_path_step = None

        if self.movement_type == MovementType.EXIT:
            self.set_exit_as_goal()
            

    def move_step(self):
        if self.movement_type == MovementType.RANDOM:
            self.random_walk_movement()

        elif self.movement_type == MovementType.EXIT:
            self.exit_through_door_movement()

    def random_walk_movement(self):
        if self.next_path_step:
            self.move_to_goal()

        else:
            walk_check = self.random.random()
            if walk_check < self.model.move_probability:

                self.goal = self.model.navmesh.get_random_node()
                self.path = self.model.navmesh.astar_search(self.pos, self.goal)
                self.set_next_path_step()
                self.move_to_goal()

    def exit_through_door_movement(self):
        if self.next_path_step:
            self.move_to_goal()
        
        else:
            # self.model.schedule.remove(self)
            self.model.grid.remove_agent(self)
            self.render = False

    def move_to_goal(self):
        # If the next node is not the final node
        if len(self.path) > 0:
            # if the distance between the agent and the node is small
            if h.euclidean_distance(self.pos, self.next_path_step) < self.model.next_node_offset:
                # theen take the next node as target before moving
                self.set_next_path_step()
        
        # if the next node is the final node
        else:
            # if arrived, don't move
            if h.euclidean_distance(self.pos, self.goal) < 0.02:
                self.set_next_path_step()
                return

        new_pos = self.compute_new_position(self.next_path_step)
        x, y = new_pos

        self.model.grid.move_agent(self, new_pos)

    def set_next_path_step(self):
        if len(self.path) > 0:
            self.next_path_step = self.path[0]
            self.path.pop(0)
        else:
            self.next_path_step = None

    def set_exit_as_goal(self):
        exit_x, exit_y = tuple(self.model.navmesh.shape["exits"][0])
        self.goal = (exit_x, exit_y)
        self.path = self.model.navmesh.astar_search(self.pos, self.goal)
        self.set_next_path_step()

    def compute_new_position(self, target):
        pos = np.array(self.pos)
        target = np.array(target)

        # Compute the distance
        distance_vector = target - pos

        # normalize the vector as unit vector
        normalized_distance_vector = h.unit_vector(distance_vector)

        # multiply by velocity
        movement_vector = normalized_distance_vector * self.model.move_velocity

        # check if the movement amount doesn't take too far
        if(np.any(np.abs(distance_vector) < np.abs(movement_vector))):
            new_pos = target.astype(np.float)
        else:
            new_pos = pos + movement_vector

        # NOT REQUIRED ANYMORE, TO KEEP IN CASE
        # force to stay inbounds 
        # new_x, new_y = tuple(new_pos)

        # # the max is considered out of bounds
        # new_x = min(max(0, new_x), self.model.grid_width - 0.01)
        # # the max is considered out of bounds
        # new_y = min(max(0, new_y), self.model.grid_height - 0.01)

        # return new_x, new_y

        return tuple(new_pos)
