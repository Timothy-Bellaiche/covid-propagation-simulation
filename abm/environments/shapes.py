import numpy as np

def complex_shape():
    points = [
        (0, 0),
        (2, 0),
        (2, 2),
        (3, 4),
        (5, 4),
        (6, 2),
        (6, 0),
        (10, 0),
        (14, 0),
        (14, 4),
        (12, 4),
        (12, 0.5),
        (10, 0.5),
        (10, 10),
        (0, 10)
    ]

    segments = []

    for i in range(len(points) - 1):
        segments.append((i, i+1))

    segments.append((len(points) - 1, 0))

    hole_points = ([
        (2, 8),
        (6, 8),
        (6, 6),
        (2, 6)
    ])

    # a point inside the hole
    hole_marker = [[3, 7.5]]

    points.extend(hole_points)

    for i in range(len(hole_points) - 1):
        segments.append((i + len(points) - len(hole_points), i + len(points) - len(hole_points) + 1))

    segments.append((len(points) - len(hole_points), len(points) - 1))

    exits = [(6, 1)]

    points.extend(exits)


    points_of_interest = [
        make_point_of_interest("Coffee", (12.5, 3.5)), 
        make_point_of_interest("Coffee", (13.5, 3.5)),
        make_point_of_interest("Door", (10, 0.25))
    ]

    for poi in points_of_interest:
        points.append(poi["position"])

    width = 0
    height = 0

    for x, y in points:
        if x > width:
            width = x
        if y > height:
            height = y

    points = np.array(points)
    segments = np.array(segments)
    exits = np.array(exits)
    points_of_interest = np.array(points_of_interest)

    shape = dict(
        vertices=points, 
        segments=segments, 
        holes=hole_marker, 
        exits=exits,
        points_of_interest=points_of_interest,
        width=width,
        height=height
    )

    return shape

def make_point_of_interest(name, position, color="rgba(255, 105, 105, 1)"):
    x, y = position
    return {
        "name": name,
        "position": position,
        "x": x,
        "y": y,
        "color": color
    }