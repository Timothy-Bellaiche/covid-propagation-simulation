from model.model import CovidContagion
from model.parameters import Parameters, shape_parameters_for_model_simulations
import numpy as np
import matplotlib.pyplot as plt
import dash
import dash_table
import pandas as pd
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import plotly.graph_objs as go
import pathlib
import os
import pandas as pd
import numpy as np
import re
import pickle 
from dash_table import DataTable



parameters = Parameters()

# here make parameters vary from defaults
parameters.n_pupils["value"] = 20

formated_parameters = shape_parameters_for_model_simulations(parameters) # create **kwargs input for model init

model = CovidContagion(**formated_parameters) 

model.run_model(step_count=30)

df = model.datacollector.get_model_vars_dataframe()
print(df.columns)
print(df.iloc[-1]['Infection Origins'],type(df.iloc[-1]['Infection Origins']))
print(list(df.iloc[-1]['Infection Origins'].keys()),list(df.iloc[-1]['Infection Origins'].values()))
#plt.figure()
#df = df.drop(["Infection Origins"], axis=1) # remove since not plotable
#df.plot()
#plt.show()


#df = pd.read_csv('dataset_final.csv')
app = dash.Dash(
    __name__,
    meta_tags=[{"name": "viewport", "content": "width=device-width, initial-scale=1"}],
)
server = app.server
app.config["suppress_callback_exceptions"] = True

APP_PATH = str(pathlib.Path(__file__).parent.resolve())
colors = {
    'background': 'black',
    'text': 'white'
}
markdown_text1 = '''
#### Prediction of the covid19 epidemic propagation in a school. 
We used Agent-based-modelling to predict the propagation of the epidemic in a school. 
'''
liste_col_a_plot=['Infected Ratio', 'Hands Infected Ratio', 'Infection Origins']

def build_banner():
    return html.Div(
        id="banner",
        className="banner",
        children=[
            html.Div(
                id="banner-text",
                children=[
                    html.H5("Epidemic propagation in school"),
                    html.H6("Agent-based-model simulations"),
                ],
            ),
            
            html.Div(
                id="banner-logo",
                children=[
                    html.Button(
                        id="learn-more-button", children="LEARN MORE", n_clicks=0
                    ),
                    html.Img(id="logo", src=app.get_asset_url("logo_Capgemini.jpg")),
                ],
            ),
        ],
    )


app.layout = html.Div(
        id="big-app-container",
        children=[
            build_banner(),
            # header
            html.Div([
            dcc.Markdown(children=markdown_text1)
            ],style= {'color':'white'}),
            
            html.Div([
            dcc.Dropdown(id='yaxis',
                         options = [{'label': 'Simulation with mask', 'value': 'Simulation with mask'},
                                    {'label': 'Simulation withtout mask', 'value': 'Simulation without mask'}],
                         value = 'Simulation withtout mask')#
                         
            ],style={'width':'100%'}),#
           
            
            html.Div([
                html.Div([
                html.H3('Infected ratio'),
                dcc.Graph(id='g1', figure={
            'data': [
                dict(x=df.index.values,
                y=df[i],
                #text=df_by_continent['country'],
                mode='lines',
                opacity=0.7,
                marker={
                'size': 15,
                'line': {'width': 0.5, 'color': 'white'}
                },
                    name=i
                ) for i in ['Infected Ratio','Hands Infected Ratio']
            ],
            'layout': dict(
                xaxis={'title': 'Time step'},
                yaxis={'title': 'Infected ratio'},
                height=500, # px
                width=500
                )
            
        })], className="six columns"),

                html.Div([
                html.H3('Number of infected'),
                dcc.Graph(id='g2', figure={
                'data':[dict(x=list(df.iloc[-1]['Infection Origins'].keys()), 
                y=list(df.iloc[-1]['Infection Origins'].values()), 
                type='bar')],
                'layout': dict(
                xaxis={'title': 'Categories'},
                yaxis={'title': 'Number of infected'},
                height=500, # px
                width=500)
                }
        
        )
                ], className="six columns"),
            ], className="row")
            ]
            )


if __name__ == '__main__':
    app.run_server(debug=True, port=8053)