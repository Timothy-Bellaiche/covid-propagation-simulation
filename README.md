Works with python 3.7  

To create the virtual environment with conda, go to dir and type
```
conda env create -f environment.yml -n covid-propagation
```

To run the visualization platform, go to root folder, activate the virtual env and type
```
python run-visualization.py
```